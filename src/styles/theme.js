const theme = {
  colors: {
    primary: "#00AFDA",
    secondary: "#ECECEC",
    grey: 'rgba(196, 196, 196, 0.17)'
  },
};

export default theme;
