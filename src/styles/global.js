import React from "react";
import { Global, css } from "@emotion/core";

const globalStyle = () => (
  <>
    <Global
      styles={css`
        /* * {
          border: 1px solid black;
          margin: 1px;
        } */

        html {
          min-width: 350px;
        }

        body: {
          /* display: flex; */
          min-width: 350px;
          /* background-color: black; */
          min-height: 100vh;

        }

        #root {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
          min-width: 350px;
          min-height: 100vh;
        }
      `}
    />
  </>
);

export default globalStyle;
