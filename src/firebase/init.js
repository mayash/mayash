import firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/firestore'

export const app = firebase.initializeApp({
  apiKey: "AIzaSyDu75sPA-73yeB6YCrXq6rE-sKxlJwWKWk",
  authDomain: "mayash-io.firebaseapp.com",
  databaseURL: "https://mayash-io.firebaseio.com",
  projectId: "mayash-io",
  storageBucket: "mayash-io.appspot.com",
  messagingSenderId: "994280172327",
  appId: "1:994280172327:web:bf57af8a5ec4a04b661702"
})

export default firebase;
