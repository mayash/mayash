import firebase from './init';

const db = firebase.firestore();

export const COLLECTIONS = {
  users: 'users',

  circles: 'circles',
  circleCollections: {
    departments: 'departments',
    members: 'members'
  },

  courses: 'courses',
  // This table will keep track of usernames to users and circles
  usernames: 'usernames',

  classrooms: 'classrooms',
  classroomCollections: {
    notes: 'notes',
    messages: 'messages', // discussions
    questions: 'questions', // tests
    questionCollections: {
      answers: 'answers'
    }
  }
};

export default db;
