import firebase from './init';

const auth = firebase.auth();

// FirebaseUI config.
export const firebaseUiAuthConfig = {
  signInSuccessUrl: '/',
  signInOptions: [
    {
      provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
      recaptchaParameters: {
        type: 'image', // 'audio'
        size: 'invisible', // 'normal' or 'compact'
        badge: 'bottomleft' //' bottomright' or 'inline' applies to invisible.
      },
      defaultCountry: 'IN',
      whitelistedCountries: ['IN', '+91']
    },
  ],
  tosUrl: '/terms-of-services',
  privacyPolicyUrl: function () {
    window.location.assign('/privacy-policy');
  }
};

export default auth;
