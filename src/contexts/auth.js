import React from 'react'

const auth = React.createContext({
  auth: null
});

export default auth;
