import React from 'react'

const user = React.createContext({
  user: null
});

export default user;
