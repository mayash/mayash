import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";

import AppRoutes from "./AppRoutes";

function App() {
  return (
    <>
      <CssBaseline />
      <AppRoutes />
    </>
  );
}

export default App;
