import React from 'react'

// import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { Card, Row, Col } from 'react-bootstrap'

import useCircles from '../../hooks/useCircles';
import Loading from '../Loading';

const Circles = () => {
  const { loading, error, circles } = useCircles();

  if (loading || !circles) {
    return <Loading />
  }

  if (error) {
    return <div>error...</div>
  }

  if (!circles) {
    return <div>Not Found</div>
  }

  return (
    <Row
      style={{
        padding: '4px'
      }}
    >
      <Col>
        {circles.map(({ username, circleId, title }, index) => (
          <Link
            key={index}
            to={`/circles/${circleId}`}
            style={{ textDecoration: 'none', margin: '4px' }}
          >
            <Card>
              <Card.Body>
                <Card.Title>
                  {title}
                </Card.Title>
              </Card.Body>
            </Card>
          </Link>
        ))}
      </Col>
    </Row>
  );
}

export default Circles;
