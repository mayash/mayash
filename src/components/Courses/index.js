import React from 'react'

// import styled from '@emotion/styled';

import useCourses from '../../hooks/useCourses'

const Courses = () => {
  const { loading, error, courses } = useCourses();

  if (error) {
    return <div>error...</div>
  }

  if (loading || !courses) {
    return <div>loading...</div>
  }

  return (
    <div>
      {courses.map(({ title }, index) => (
        <div key={index}>{title}</div>
      ))}
    </div>
  )
}

export default Courses;
