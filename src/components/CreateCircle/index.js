import React, { useState } from 'react'

// import styled from '@emotion/styled';

import { Card, Form, Button, InputGroup, FormControl } from 'react-bootstrap';

import database, { COLLECTIONS } from '../../firebase/database';

const CreateCircle = () => {
  const [title, setTitle] = useState('');

  const onSubmit = e => {
    e.preventDefault();

    const newCircleRef = database.collection(COLLECTIONS.circles).doc();

    newCircleRef.set({
      circleId: newCircleRef.id,
      title,
    }).then(docRef => {
      // console.log('Success, ', docRef.id);
      setTitle('');
    }).catch(error => {
      console.error(error)
    });
  };

  return (
    <Card>
      <Card.Body>
        <Form onSubmit={onSubmit} inline>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="Circle Name"
              aria-describedby="basic-addon2"

              placeholder="Circle Name"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <InputGroup.Append>
              <Button variant="primary" type="submit">Button</Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Card.Body>
    </Card>
  );
}

export default CreateCircle;
