import React from "react";
import styled from "@emotion/styled";

const Container = styled.footer`
  display: flex;
  flex-direction: column;
  align-content: center;
  align-items: flex-start;
  justify-content: center;

  background: #00afda;
  border-radius: 10px 10px 0px 0px;

  padding-top: 68px;
  padding-left: 40px;
  padding-bottom: 40px;
  padding-right: 40px;

  > div {
    
    
    h2 {
      font-family: Roboto;
      font-style: normal;
      font-weight: 900;
      font-size: 21px;
      line-height: 18px;

      color: #ffffff;
    }

    p {
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 19px;

      color: #fffcfc;
    }

    ul {
      /*  */
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 19px;

      color: #fffcfc;

      padding-inline-start: 0px;

      li {
        list-style-type: none;
      }
    }
  }

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: space-around;
    align-items: center;

    > div {
      width: 30%;
      margin: 12px;
    }
  }
`;

const Footer = () => {
  return (
    <Container>
      <div>
        <h2>@hbarve1</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
        </p>
      </div>

      <div>
        <h2>Contact me</h2>
        <ul>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
        </ul>
      </div>

      <div>
        <h2>My Work</h2>
        <ul>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
        </ul>
      </div>

      <div>
        <h2>Important Links</h2>
        <ul>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
          <li>Lorem ipsum</li>
        </ul>
      </div>
    </Container>
  );
};

export default Footer;
