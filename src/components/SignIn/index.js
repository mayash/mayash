import React, { useEffect } from "react";
// import styled from "@emotion/styled";

import * as firebaseui from 'firebaseui';

import firebaseAuth, { firebaseUiAuthConfig } from '../../firebase/auth';

import Container from './styles/Container'
import Title from './styles/Title';

const SignIn = () => {

  useEffect(() => {
    // Initialize the FirebaseUI Widget using Firebase.
    const ui = new firebaseui.auth.AuthUI(firebaseAuth());
    // The start method will wait until the DOM is loaded.
    ui.start('#firebaseui-auth-container', firebaseUiAuthConfig);
  });

  return (
    <Container>

      <Title>Sign In</Title>

      <div id="firebaseui-auth-container" />

    </Container>
  );
};

export default SignIn;
