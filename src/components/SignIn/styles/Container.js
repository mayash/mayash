import styled from "@emotion/styled";

const Container = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  align-items: center;
  justify-items: center;

  background-color: grey;
  min-height: calc(100vh - 70px);
`;

export default Container;
