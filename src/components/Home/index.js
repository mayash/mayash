import React from "react";
// import styled from "@emotion/styled";

import { Container } from 'react-bootstrap';

import useAuth from '../../hooks/useAuth';
import SignIn from '../SignIn';

import Loading from '../Loading';
import Circles from '../Circles';

const Home = () => {
  const { initializing, auth } = useAuth();

  if (initializing) {
    return <Loading />
  }

  if (!auth) {
    return <SignIn />
  }

  return (
    <Container>
      <Circles />
    </Container>
  );
};

export default Home;
