import React from 'react'

// import styled from '@emotion/styled';
import { Card, Row, Col } from 'react-bootstrap'
// import { Link } from 'react-router-dom';

import useDepartments from '../../hooks/useDepartments';
import Loading from '../Loading'

const Departments = ({ circleId }) => {
  const { loading, error, departments } = useDepartments({ circleId });

  if (loading) {
    return <Loading />
  }

  if (error) {
    return <div>error...</div>
  }

  if (!departments) {
    return <div>Not Found</div>
  }

  return (
    <Row
      style={{
        padding: '4px'
      }}
    >
      <Col>
        {departments.map(({ id, title }, index) => (
          <Card key={id}>
            <Card.Body>
              <Card.Title>
                {title}
              </Card.Title>
            </Card.Body>
          </Card>
        ))}
      </Col>
    </Row>
  )
}

export default Departments;
