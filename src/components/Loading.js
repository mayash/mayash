import React from "react";

import { Container, Spinner, Row, Col } from 'react-bootstrap';

const Loader = () => {
  return (
    <Container style={{
      display: 'flex',
      justifyContent: 'center',
    }}>
      <Row>
        <Col>
          <Spinner animation="border" variant="primary" />
        </Col>
      </Row>
    </Container >
  );
};

export default Loader;
