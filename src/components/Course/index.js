import React from "react";
// import styled from "@emotion/styled";

import useCourse from '../../hooks/useCourse'

const Course = () => {
  const { error, loading, course } = useCourse('2CzaLGCpGVm7LzHkb3Qe');

  if (error) {
    return <div>error...</div>
  }

  if (loading || !course) {
    return <div>loading...</div>
  }

  return (
    <div>
      {course.title}
    </div>
  )
};

export default Course;
