import React, { useState } from 'react'

// import styled from '@emotion/styled';

import { Card, Form, Button, InputGroup, FormControl } from 'react-bootstrap';

import database, { COLLECTIONS } from '../../firebase/database';

const AddCircleMember = ({ circleId }) => {
  const [title, setTitle] = useState('');

  const onSubmit = e => {
    e.preventDefault();

    const newDepartmentRef = database
      .collection(COLLECTIONS.circles)
      .doc(circleId)
      .collection(COLLECTIONS.circleCollections.departments)
      .doc()

    newDepartmentRef.set({
      id: newDepartmentRef.id,
      title,
    }).then(docRef => {
      // console.log('Success, ', docRef.id);
      setTitle('');
    }).catch(error => {
      console.error(error)
    });
  };

  return (
    <Card>
      <Card.Body>
        <Form onSubmit={onSubmit} inline>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="Department Name"
              aria-describedby="basic-addon2"

              placeholder="Department Name"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <InputGroup.Append>
              <Button variant="primary" type="submit">Button</Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Card.Body>
    </Card>
  );
}

export default AddCircleMember;
