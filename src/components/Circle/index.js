import React, { useState } from "react";
// import styled from '@emotion/styled';

import { Container, Card, Tabs, Tab } from 'react-bootstrap'

import { useParams } from "react-router";
import useCircle from '../../hooks/useCircle';
import CreateClassroom from '../CreateClassroom';
import Classrooms from '../Classrooms';
import CreateDepartment from '../CreateDepartment';
import Departments from '../Departments';
import Loading from '../Loading';

import CircleMembers from './CircleMembers';


const Circle = () => {
  const { circleId } = useParams()
  const { error, loading, circle } = useCircle(circleId);
  const [key, setKey] = useState('departments');

  if (error) {
    return <div>error...</div>
  }

  if (loading) {
    return <Loading />
  }

  if (!circle) {
    return <div>Not Found</div>
  }

  return (
    <Container>
      <Card>
        <Card.Img variant="top" src="https://cdn.shortpixel.ai/client/to_webp,q_glossy,ret_img,w_2400/https://blog.snappa.com/wp-content/uploads/2017/01/facebook-cover-photo-size.png" />
      </Card>
      <Tabs id="circle-tabs" activeKey={key} onSelect={k => setKey(k)}>
        <Tab eventKey="home" title="Home" disabled>
          hi there
        </Tab>
        <Tab eventKey="departments" title="Departments">
          <CreateDepartment circleId={circleId} />
          <Departments circleId={circleId} />
        </Tab>
        <Tab eventKey="members" title="Members">
          <CircleMembers circleId={circleId} />
        </Tab>
        <Tab eventKey="classrooms" title="Classrooms" >
          <CreateClassroom circleId={circleId} />
          <Classrooms circleId={circleId} />
        </Tab>
      </Tabs>
    </Container>
  )
};

export default Circle;
