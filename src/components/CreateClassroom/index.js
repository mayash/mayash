import React, { useState } from 'react'

// import styled from '@emotion/styled';

import database, { COLLECTIONS } from '../../firebase/database'
import { Card, Form, Button, InputGroup, FormControl } from 'react-bootstrap';

const CreateCourse = ({ circleId }) => {
  const [title, setTitle] = useState('')

  const onSubmit = e => {
    e.preventDefault();

    const newClassroomRef = database.collection(COLLECTIONS.classrooms).doc();

    newClassroomRef.set({
      classroomId: newClassroomRef.id,
      circleId,
      title
    }).then(docRef => {
      // console.log('Success, ', docRef.id);
      setTitle('');
    }).catch(error => {
      console.error(error)
    });
  };

  return (
    <Card>
      <Card.Body>
        <Form onSubmit={onSubmit} inline>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="Classroom Name"
              aria-describedby="basic-addon2"

              placeholder="Classroom Name"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <InputGroup.Append>
              <Button variant="primary">Button</Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Card.Body>
    </Card>
  )
}

export default CreateCourse;
