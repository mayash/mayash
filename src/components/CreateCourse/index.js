import React, { useState } from 'react'

import styled from '@emotion/styled';

import database, { COLLECTIONS } from '../../firebase/database'

const Form = styled.form``;

const CreateCourse = () => {
  const [title, setTitle] = useState('')

  return (
    <div>
      <Form onSubmit={e => {
        e.preventDefault();

        const newCourseRef = database.collection(COLLECTIONS.courses).doc();

        newCourseRef.set({
          courseId: newCourseRef.id,
          title
        }).then(docRef => {
          // console.log('Success, ', docRef.id);
          setTitle('');
        }).catch(error => {
          console.error(error)
        });
      }}>
        <input
          placeholder="Course Name"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <button type="submit">Submit</button>
      </Form>
    </div>
  )
}

export default CreateCourse;
