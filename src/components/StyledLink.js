import styled from "@emotion/styled";
import { Link } from "react-router-dom";


const StyledLink = styled(Link)`
  width:178px;

  background: ${({ isPrimary, theme }) => isPrimary ? theme.colors.primary : theme.colors.secondary};
  border-radius: 4.83019px;

  font-family: Roboto;
  font-style: normal;
  font-weight: 900;
  font-size: 16px;

  color: ${({ isPrimary }) => isPrimary ? '#FFFFFF' : '#545454'};

  box-shadow: 0px 8px 17px rgba(0, 0, 0, 0.2);
  padding: 7px 0px;
  border-radius: 4px;

  text-decoration: none;
  text-align: center;

  @media (min-width: 768px) {
    width: 215px;
    padding: 15px 0px;
    border-radius: 10px;
    font-size: 20px;

  }

`;

export default StyledLink;
