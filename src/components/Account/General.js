import React, { useState } from "react";
// import styled from '@emotion/styled';
// import { Redirect } from 'react-router'

import { Card, Form, Button, Row, Col } from 'react-bootstrap'

import database, { COLLECTIONS } from '../../firebase/database'

const General = ({ user }) => {
  const { id, phoneNumber } = user;

  const [name, setName] = useState(user.name || '');
  const [email, setEmail] = useState(user.email || '');
  const [status, setStatus] = useState(user.status || '');

  return (
    <Card>
      <Card.Body>
        <Form
          onSubmit={e => {
            e.preventDefault();

            database.collection(COLLECTIONS.users).doc(id).update({ name })
          }}
        >
          <Row>
            <Col xs={8} sm={8} md={10} lg={10}>
              <Form.Group controlId="user-full-name">
                <Form.Control
                  type="text"
                  placeholder="Full Name"
                  value={name}
                  onChange={e => setName(e.target.value)}
                  required
                  minLength={5}
                  maxLength={35}
                />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="primary" type="submit">
                Save
              </Button>
            </Col>
          </Row>
        </Form>
      </Card.Body>

      <Card.Body>
        <Form
          onSubmit={e => {
            e.preventDefault();

            database.collection(COLLECTIONS.users).doc(id).update({ email })
          }}
        >
          <Row>
            <Col xs={8} sm={8} md={10} lg={10}>
              <Form.Group controlId="user-name">
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="primary" type="submit">
                Save
              </Button>
            </Col>
          </Row>
        </Form>
      </Card.Body>

      <Card.Body>
        <Form>
          <Row>
            <Col xs={8} sm={8} md={10} lg={10}>
              <Form.Group controlId="user-phone-number">
                <Form.Control type="text" placeholder="Mobile Number" disabled value={phoneNumber} readOnly />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="primary" type="submit" disabled>
                Save
              </Button>
            </Col>
          </Row>
        </Form>
      </Card.Body>

      <Card.Body>
        <Form
          onSubmit={e => {
            e.preventDefault();

            database.collection(COLLECTIONS.users).doc(id).update({ status })
          }}
        >
          <Row>
            <Col xs={8} sm={8} md={10} lg={10}>
              <Form.Group controlId="user-status">
                <Form.Control
                  as="textarea"
                  aria-label="With textarea"
                  rows={3}
                  placeholder="About you"
                  value={status}
                  onChange={e => setStatus(e.target.value)}
                  minLength={10}
                  maxLength={300}
                />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="primary" type="submit">
                Save
              </Button>
            </Col>
          </Row>
        </Form>
      </Card.Body>
    </Card>
  );
};

export default General;
