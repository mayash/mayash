import React, { useState } from "react";
// import styled from '@emotion/styled';
import { Redirect } from 'react-router'

import { Container, Card, Tabs, Tab } from 'react-bootstrap'

import Loading from '../Loading';

import UserContext from '../../contexts/user'
import General from './General'

const Circle = () => {
  const [key, setKey] = useState('general');

  return (
    <UserContext.Consumer>
      {({ user }) => {

        return (
          <Container>
            {/* <Card>
              <Card.Img variant="top" src="https://cdn.shortpixel.ai/client/to_webp,q_glossy,ret_img,w_2400/https://blog.snappa.com/wp-content/uploads/2017/01/facebook-cover-photo-size.png" />
            </Card> */}
            <Tabs id="circle-tabs" activeKey={key} onSelect={k => setKey(k)}>
              <Tab eventKey="general" title="General">
                <General user={user} />
              </Tab>
              <Tab eventKey="members" title="Education" disabled>
                <div>Education Circle List</div>
              </Tab>
              <Tab eventKey="classrooms" title="Account Type" disabled>
                <div>Pricing</div>
              </Tab>
            </Tabs>
          </Container>
        )
      }}
    </UserContext.Consumer>
  )
};

export default Circle;
