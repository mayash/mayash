import React, { useState } from "react";
// import styled from "@emotion/styled";
import { Redirect } from 'react-router';

import { Tabs, Tab } from 'react-bootstrap'

import CreateCircle from '../CreateCircle'
import Circles from '../Circles'
import Users from '../Users';

import UserContext from '../../contexts/user'

const AdminPanel = () => {
  const [key, setKey] = useState('users');

  return (
    <UserContext.Consumer>
      {({ user }) => {

        if (!user.roles && !user.roles.isAdmin) {
          return <Redirect to='/' />
        }

        return (
          <Tabs id="admin-panel-tabs" activeKey={key} onSelect={k => setKey(k)}>
            <Tab eventKey="users" title="Users">
              <Users />
            </Tab>

            <Tab eventKey="circles" title="Circles">
              <>
                <CreateCircle />
                <Circles />
              </>
            </Tab>
          </Tabs>
        );
      }}
    </UserContext.Consumer>
  );
};

export default AdminPanel;
