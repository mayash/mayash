import React, { useState } from "react";
// import styled from '@emotion/styled';

// import EditorJS from '@editorjs/editorjs';
// import Header from '@editorjs/header';
// import List from '@editorjs/list';

import { Container, Card, Tabs, Tab } from 'react-bootstrap';


const Classroom = () => {
  const [key, setKey] = useState('home');

  // let editor = new EditorJS({
  //   holderId: 'codex-editor',
  //   placeholder: 'Let`s write an awesome story!'
  // });


  return (
    <Container>
      <Card>
        <Card.Img variant="top" src="https://cdn.shortpixel.ai/client/to_webp,q_glossy,ret_img,w_2400/https://blog.snappa.com/wp-content/uploads/2017/01/facebook-cover-photo-size.png" />
      </Card>
      <Tabs id="classroom-tabs" activeKey={key} onSelect={k => setKey(k)}>
        <Tab eventKey="home" title="Home" disabled>
          hi there
        </Tab>
        <Tab eventKey="notes" title="Notes" disabled>
          hi there
        </Tab>
        <Tab eventKey="discussion" title="Discussion" disabled>
          hi there
        </Tab>
        <Tab eventKey="tests" title="Tests" disabled>
          hi there
        </Tab>
        <Tab eventKey="members" title="Members" disabled>
          hi there
        </Tab>
        <Tab eventKey="reviews" title="Reviews" disabled>
          hi there
        </Tab>
      </Tabs>
    </Container>
  );
};

export default Classroom;
