import React from "react";
import { useHistory } from 'react-router';

import { Navbar, Nav } from 'react-bootstrap';
import AuthContext from '../contexts/auth';

const Header = () => {
  let history = useHistory();


  return (
    <Navbar bg="primary" variant="dark">
      <Navbar.Brand href="/">Mayash</Navbar.Brand>
      {/* <Navbar.Toggle /> */}
      <Nav className="mr-auto"></Nav>
      <AuthContext.Consumer>
        {({ auth }) => (
          <Nav className="justify-content-end">
            {auth ? (
              <>
                <Nav.Item>
                  <Nav.Link
                    href="#"
                    onClick={e => {
                      e.preventDefault();
                      history.push('/account')
                    }}
                  >
                    Account
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="#home">Sign Out</Nav.Link>
                </Nav.Item>
              </>
            ) : (
                <Nav.Item>
                  <Nav.Link href="#home">Sign In</Nav.Link>
                </Nav.Item>
              )}
          </Nav>
        )}
      </AuthContext.Consumer>
    </Navbar>
  );
};

export default Header;
