import React from 'react'

// import styled from '@emotion/styled';
import { Link } from 'react-router-dom'

import useClassrooms from '../../hooks/useClassrooms'

import { Card, Row, Col } from 'react-bootstrap';
import Loading from '../Loading'


const Classrooms = ({ circleId }) => {
  const { loading, error, classrooms } = useClassrooms({ circleId });

  if (error) {
    return <div>error...</div>
  }

  if (loading) {
    return <Loading />
  }

  if (!classrooms) {
    return <div>Not Found</div>
  }

  return (
    <Row
      style={{
        padding: '4px'
      }}
    >
      <Col>
        {classrooms.map(({ classroomId, title }, index) => (
          <Link
            key={index}
            to={`/classrooms/${classroomId}`}
            style={{ textDecoration: 'none', margin: '4px' }}
          >
            <Card>
              <Card.Body>
                <Card.Title>
                  {title}
                </Card.Title>
              </Card.Body>
            </Card>
          </Link>
        ))}
      </Col>
    </Row>
  );
}

export default Classrooms;
