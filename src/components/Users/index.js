import React from 'react'

// import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { Card, Row, Col } from 'react-bootstrap'

import useUsers from '../../hooks/useUsers';

import Loading from '../Loading';

const Users = () => {
  const { loading, error, users } = useUsers();


  if (error) {
    return <div>error...</div>
  }

  if (loading) {
    return <Loading />
  }

  if (!users) {
    return <div>Not Found</div>
  }

  return (
    <Row
      style={{
        padding: '4px'
      }}
    >
      <Col>
        {users.map(({ id, phoneNumber, name, email }, index) => (
          <Link
            key={index}
            to={`/users/${id}`}
            style={{ textDecoration: 'none', margin: '4px' }}
          >
            <Card>
              <Card.Body>
                <Card.Title>
                  {phoneNumber}
                </Card.Title>
              </Card.Body>
            </Card>
          </Link>
        ))}
      </Col>
    </Row>
  );
}

export default Users;
