import React from "react";
import { Router, Switch, Route, } from "react-router";

import { createBrowserHistory } from "history"

import AppLayout from "./AppLayout";

import Home from "./pages/Home";
import SignIn from "./pages/SignIn";
import Classroom from "./pages/Classroom";
import Course from "./pages/Course";
import Account from "./pages/Account";
import Admin from "./pages/Admin";
import ErrorPage from "./pages/ErrorPage";
import Circle from "./components/Circle";

const history = createBrowserHistory()

// @NOTE: use react-router hooks -> https://reacttraining.com/blog/react-router-v5-1/

function AppRoutes() {
  return (
    <Router history={history}>
      <AppLayout>
        <Switch>
          <Route path="/" exact component={Home} />

          <Route path="/signin" exact component={SignIn} />

          <Route path="/circles/:circleId" exact>
            <Circle />
          </Route>

          <Route path="/classrooms/:classroomId" exact>
            <Classroom />
          </Route>

          <Route path="/courses/:courseId" exact component={Course} />
          <Route path="/account" exact component={Account} />

          <Route path="/admin" exact component={Admin} />

          <Route component={ErrorPage} />
        </Switch>
      </AppLayout>
    </Router>
  );
}

export default AppRoutes;
