import React from "react";

import { ThemeProvider } from "emotion-theming";
import { Redirect } from 'react-router';

import theme from "./styles/theme";
import GlobalTheme from "./styles/global";

import Header from "./components/Header";
// import Footer from "./components/Footer";
import Loading from "./components/Loading";
import AuthContext from './contexts/auth';
import UserContext from './contexts/user';
import useAuth from './hooks/useAuth';
import useUser from './hooks/useUser';

function Child({ children, id }) {
  const { error, loading, user } = useUser({ id });

  return (
    <UserContext.Provider value={{ user }}>
      <Header />
      {loading && <Loading />}
      {error && <div>Something went wrong, please try again.</div>}
      {!loading && !error && user && children}
      {/* <Footer /> */}
    </UserContext.Provider>
  )
}


function AppLayout({ children }) {
  const { initializing, auth } = useAuth();

  return (
    <ThemeProvider theme={theme}>
      <AuthContext.Provider value={{ auth }}>
        <>
          <GlobalTheme />

          {initializing && <Loading />}

          {!initializing && !auth && <Redirect to='/' />}

          {!initializing && auth && (
            <AuthContext.Consumer>
              {({ auth }) => {
                return <Child children={children} id={auth.uid} />
              }}
            </AuthContext.Consumer>
          )}
        </>
      </AuthContext.Provider>
    </ThemeProvider>
  );
}

export default AppLayout;
