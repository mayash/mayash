import React from 'react';

import firebase from '../firebase/init';
import database, { COLLECTIONS } from '../firebase/database';

export const useAuth = () => {
  const [state, setState] = React.useState(() => {
    const auth = firebase.auth().currentUser;

    return {
      initializing: !auth,
      auth
    }
  })


  function onChange(auth) {
    setState({ initializing: false, auth })
  }

  React.useEffect(() => {
    // listen for auth state changes
    const unsubscribe = firebase.auth().onAuthStateChanged(onChange);

    // we are not using signup and auth is provided my firebase, it is creating and
    // account in firebase authentication but not in database, and also we are using firebaseui
    // where we are not customising anything, so we can't create user account in database,
    // so we are adding this code for checking if user's data exists in 'users' table or not,
    // if not, this code will create an account.
    if (state.auth) {
      database.collection(COLLECTIONS.users)
        .doc(state.auth.uid)
        .get()
        .then(querySnapshot => {
          // console.log(querySnapshot.data())
          if (!querySnapshot.data()) {
            database.collection(COLLECTIONS.users).doc(state.auth.uid).set({
              id: state.auth.uid,
              phoneNumber: state.auth.phoneNumber
            })
          }
        }).catch(error => {
          console.log(error);
        });
    }
    // unsubscribe to the listener when unmounting
    return () => unsubscribe()
  }, [state.auth])

  return state;
}

export default useAuth;
