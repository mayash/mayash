/**
 * Reference: https://dev.to/bmcmahen/using-firebase-with-react-hooks-21ap
 */

import React, { useEffect } from 'react';

import database, { COLLECTIONS } from '../firebase/database'

const useCircleMembers = ({ circleId }) => {
  // initialize our default state
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [members, setMembers] = React.useState(null);

  // when the id attribute changes (including mount)
  // subscribe to the members document and update
  // our state when it changes.
  useEffect(
    () => {
      const unsubscribe = database
        .collection(COLLECTIONS.circles)
        .doc(circleId)
        .collection(COLLECTIONS.circleCollections.members)
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            const temp = []
            querySnapshot.forEach(function (doc) {
              // doc.data() is never undefined for query doc snapshots
              // console.log(doc.id, " => ", doc.data());

              temp.push(doc.data())
            });
            setLoading(false);
            setMembers(temp)
          },
          err => {
            setError(err)
          })
      // returning the unsubscribe function will ensure that
      // we unsubscribe from document changes when our id
      // changes to a different value.
      return () => unsubscribe()
    },
    [circleId]
  )

  return {
    error,
    loading,
    members,
  }
}

export default useCircleMembers;
