/**
 * Reference: https://dev.to/bmcmahen/using-firebase-with-react-hooks-21ap
 */

import React, { useEffect } from 'react';

import firebase from '../firebase/init';
import { COLLECTIONS } from '../firebase/database'


const useCourses = () => {
  // initialize our default state
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [courses, setCourses] = React.useState(null)
  // when the id attribute changes (including mount)
  // subscribe to the courses document and update
  // our state when it changes.
  useEffect(
    () => {
      const unsubscribe = firebase.firestore()
        .collection(COLLECTIONS.courses)
        .onSnapshot(
          querySnapshot => {
            const temp = []
            querySnapshot.forEach(function (doc) {
              // doc.data() is never undefined for query doc snapshots
              // console.log(doc.id, " => ", doc.data());

              temp.push(doc.data())
            });
            setLoading(false);
            setCourses(temp)
          },
          err => {
            setError(err)
          })
      // returning the unsubscribe function will ensure that
      // we unsubscribe from document changes when our id
      // changes to a different value.
      return () => unsubscribe()
    },
    []
  )

  return {
    error,
    loading,
    courses,
  }
}

export default useCourses;
