/**
 * Reference: https://dev.to/bmcmahen/using-firebase-with-react-hooks-21ap
 */

import React, { useEffect } from 'react';

import firebase from '../firebase/init';
import { COLLECTIONS } from '../firebase/database'

const useUser = ({ id }) => {
  // initialize our default state
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [user, setUser] = React.useState(null);

  // when the id attribute changes (including mount)
  // subscribe to the user document and update
  // our state when it changes.
  useEffect(
    () => {
      const unsubscribe = firebase.firestore()
        .collection(COLLECTIONS.users)
        .doc(id)
        .onSnapshot(
          querySnapshot => {
            setLoading(false);
            setUser(querySnapshot.data())
          },
          err => {
            setError(err)
          });

      // returning the unsubscribe function will ensure that
      // we unsubscribe from document changes when our id
      // changes to a different value.
      return () => unsubscribe();
    },
    [id]
  );

  return {
    error,
    loading,
    user,
  }
}

export default useUser;
