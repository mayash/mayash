/**
 * Reference: https://dev.to/bmcmahen/using-firebase-with-react-hooks-21ap
 */

import React, { useEffect } from 'react';

import database, { COLLECTIONS } from '../firebase/database'

const useDepartments = ({ circleId }) => {
  // initialize our default state
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [departments, setDepartments] = React.useState(null);

  // when the id attribute changes (including mount)
  // subscribe to the departments document and update
  // our state when it changes.
  useEffect(
    () => {
      const unsubscribe = database
        .collection(COLLECTIONS.circles)
        .doc(circleId)
        .collection(COLLECTIONS.circleCollections.departments)
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            const temp = []
            querySnapshot.forEach(function (doc) {
              // doc.data() is never undefined for query doc snapshots
              // console.log(doc.id, " => ", doc.data());

              temp.push(doc.data())
            });
            setLoading(false);
            setDepartments(temp)
          },
          err => {
            setError(err)
          })
      // returning the unsubscribe function will ensure that
      // we unsubscribe from document changes when our id
      // changes to a different value.
      return () => unsubscribe()
    },
    [circleId]
  )

  return {
    error,
    loading,
    departments,
  }
}

export default useDepartments;
