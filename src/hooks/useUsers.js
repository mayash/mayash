/**
 * Reference: https://dev.to/bmcmahen/using-firebase-with-react-hooks-21ap
 */

import React, { useEffect } from 'react';

import firebase from '../firebase/init';
import { COLLECTIONS } from '../firebase/database'

const useUsers = () => {
  // initialize our default state
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [users, setUsers] = React.useState(null);

  // when the id attribute changes (including mount)
  // subscribe to the users document and update
  // our state when it changes.
  useEffect(
    () => {
      const unsubscribe = firebase.firestore()
        .collection(COLLECTIONS.users)
        .limit(10)
        .onSnapshot(
          querySnapshot => {
            const temp = []
            querySnapshot.forEach(function (doc) {
              // doc.data() is never undefined for query doc snapshots
              // console.log(doc.id, " => ", doc.data());

              temp.push(doc.data())
            });
            setLoading(false);
            setUsers(temp)
          },
          err => {
            setError(err)
          })
      // returning the unsubscribe function will ensure that
      // we unsubscribe from document changes when our id
      // changes to a different value.
      return () => unsubscribe()
    },
    []
  )

  return {
    error,
    loading,
    users,
  }
}

export default useUsers;
