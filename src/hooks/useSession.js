import React from 'react';

import userContext from '../contexts/user';

export const useSession = () => {
  const { user } = React.useContext(userContext);

  return user
}

export default useSession;
